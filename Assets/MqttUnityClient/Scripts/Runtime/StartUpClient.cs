﻿using System.Text;

using Cysharp.Threading.Tasks;

using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

using UnityEngine;

namespace MqttUnityClient
{
    public class StartUpClient : MonoBehaviour
    {
        private const string LogTag = nameof(StartUpClient);

        private ILogger _logger;

        private void Awake()
        {
            _logger = Debug.unityLogger;
        }

        //private void Start() => UniTask.Void(async () =>
        //{
        //    // Create a new MQTT client.
        //    var factory = new MqttFactory();
        //    var mqttClient = factory.CreateMqttClient();

        //    // Use TCP connection.
        //    var options = new MqttClientOptionsBuilder()
        //        .WithTcpServer("127.0.0.1", 5000)
        //        .Build();

        //    var stoppingToken = this.GetCancellationTokenOnDestroy();

        //    await mqttClient.ConnectAsync(options, stoppingToken);
        //    await mqttClient.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("my/topic").Build());

        //    mqttClient.UseApplicationMessageReceivedHandler(handler =>
        //    {
        //        var message = handler.ApplicationMessage;
        //        _logger.Log(LogType.Log, LogTag, "topic: " + message.Topic);
        //        _logger.Log(LogType.Log, LogTag, Encoding.UTF8.GetString(message.Payload));
        //    });

        //    mqttClient.UseConnectedHandler(handler => _logger.Log(LogType.Log, LogTag, "connected."));
        //    mqttClient.UseDisconnectedHandler(handler => _logger.Log(LogType.Log, LogTag, "disconnected."));

        //    await mqttClient.PublishAsync(builder => builder.WithTopic("my/topic").WithPayload("hello from unity client."), stoppingToken);

        //    await UniTask.WaitUntilCanceled(stoppingToken);

        //    await mqttClient.DisconnectAsync(stoppingToken);
        //});
    }
}
