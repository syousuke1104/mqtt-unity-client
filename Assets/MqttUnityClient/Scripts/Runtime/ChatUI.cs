﻿using System.Text;

using Cysharp.Threading.Tasks;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace MqttUnityClient
{
    public class ChatUI : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _contentText;

        [SerializeField]
        private TMP_InputField _inputField;

        [SerializeField]
        private Button _sendButton;

        private MqttService _mqttService;

        private ILogger _logger;

        private const string LogTag = nameof(ChatUI);

        private void Start()
        {
            _logger = Debug.unityLogger;

            var stoppingToken = this.GetCancellationTokenOnDestroy();

            _sendButton.onClick.AddListener(() =>
            {
                if (string.IsNullOrWhiteSpace(_inputField.text))
                {
                    _logger.LogWarning(LogTag, "input field is empty.");
                    return;
                }

                UniTask.Void(async () =>
                {
                    await _mqttService.SendAsync(Encoding.UTF8.GetBytes(_inputField.text), stoppingToken);
                    _inputField.text = "";
                });
            });

            var stringBuilder = new StringBuilder();

            _mqttService = new MqttService(_logger, "my/topic");
            _mqttService.ApplicationMessageReceived += (message) =>
            {
                UniTask.Void(async () =>
                {
                    await UniTask.SwitchToMainThread(stoppingToken);
                    stringBuilder.AppendLine(Encoding.UTF8.GetString(message.Payload));
                    _contentText.SetText(stringBuilder);
                });
            };

            UniTask.Void(async () =>
            {
                await _mqttService.StartAsync(stoppingToken);
                await _mqttService.SendAsync(Encoding.UTF8.GetBytes("hello everyone!"), stoppingToken);
            });
        }

        private void OnDestroy() => UniTask.Void(async () =>
        {
            await _mqttService.StopAsync(this.GetCancellationTokenOnDestroy());
        });
    }
}
