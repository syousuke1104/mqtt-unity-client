﻿using System;
using System.Threading;

using Cysharp.Threading.Tasks;

using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;

using UnityEngine;

namespace MqttUnityClient
{
    public class MqttService
    {
        private const string LogTag = nameof(MqttService);

        private readonly ILogger _logger;

        private readonly IMqttClient _client;

        public event Action<MqttApplicationMessage> ApplicationMessageReceived;

        public string Topic { get; }

        public MqttService(ILogger logger, string topic)
        {
            _logger = logger;
            _client = new MqttFactory().CreateMqttClient();
            ApplicationMessageReceived = delegate { };
            Topic = topic;
        }

        public async UniTask StartAsync(CancellationToken cancellationToken)
        {
            var options = new MqttClientOptionsBuilder()
                .WithTcpServer("develop.lan.sysklab.net", 5000)
                .Build();

            await _client.ConnectAsync(options, cancellationToken);

            await _client.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic(Topic).Build());

            _client.UseApplicationMessageReceivedHandler(handler =>
            {
                _logger.Log(LogTag, "received message.");
                ApplicationMessageReceived.Invoke(handler.ApplicationMessage);
            });

            _logger.Log(LogTag, "start mqtt service.");
        }

        public async UniTask SendAsync(byte[] payload, CancellationToken cancellationToken)
        {
            var message = new MqttApplicationMessageBuilder()
                .WithTopic(Topic)
                .WithPayload(payload)
                .Build();

            await _client.PublishAsync(message, cancellationToken);

            _logger.Log(LogTag, "send message.");
        }

        public async UniTask StopAsync(CancellationToken cancellationToken)
        {
            await _client.DisconnectAsync(cancellationToken);
        }
    }
}
